package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
	if (inputNumbers.contains(null)) {
	    throw new CannotBuildPyramidException();
	}
	//ArrayList c= new ArrayList();
	try {
	Collections.sort(inputNumbers);
	}
	catch (OutOfMemoryError E) {
	    throw new CannotBuildPyramidException();
	    
	}
	ArrayList c= new ArrayList(inputNumbers);
	int allfig = 1; 
	int maxfig=1;
	while (allfig<c.size()) {
	    maxfig +=1;
		allfig+=maxfig;
	}
	int figures = 1; 
	int [][] result = new int [maxfig][maxfig*2-1];
	if (allfig==c.size()) {
	    int rowlength = maxfig*2-1;
	    int zerosFirstRow = maxfig*2-2;
	    for (int row = 0; row<maxfig; row++) {
		boolean odd = true; 
        	    for (int col = 0; col<maxfig*2-1; col++) {
        		if (col<zerosFirstRow/2-row || col>=rowlength-zerosFirstRow/2+row) {
        		result[row][col] = 0; 
        	    }
                	    else if (odd) {
                		result[row][col] = (int)c.remove(0); 
                		odd = false; 
                	    }
                	    else {
                		result[row][col] = 0;
                		odd = true; 
                	    }
	}
        	    //zerosFirstRow-=1;
        	    figures+=1;
	    }
	}
	else {
	    throw new CannotBuildPyramidException();
	}
	
	for (int[] row:result) {
	    
		System.out.println (Arrays.toString(row));
	}
	
	
	
        return result;
    }
    
    public static void main(String[] args) {
    
    //List<Integer> input = Arrays.asList(1, 15, 2);
	//List<Integer> input = Arrays.asList(1, 3, 2, 9, 4, null);
    //ArrayList c= new ArrayList(input);
	List<Integer> input = Collections.nCopies(Integer.MAX_VALUE - 1, 0);
        
    PyramidBuilder p = new PyramidBuilder();
    p.buildPyramid(input);
    
    int[][] expected = new int[][]{
        {0, 0, 1, 0, 0},
        {0, 2, 0, 3, 0},
        {4, 0, 5, 0, 9}};
        System.out.println (expected.length);
    }
    
}
