package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static boolean isDigit(String s) throws NumberFormatException {
	    try {
	        Integer.parseInt(s);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}
	
	private String oneStep (String expression, String sign) {
		String statemen = expression;
		String number1 = "";
    	String number2 = "";
    	String newst = "";
    	double result=0.00; 
    	
    	
    		if (statemen.indexOf(sign)>0) {
    			int ind = statemen.indexOf(sign);
    			int i=1;
    			int m=1;
    			while (ind-i>=0 && 
    				(isDigit (Character.toString(statemen.charAt(ind-i))) 
    				|| Character.toString(statemen.charAt(ind-i)).equals("."))) {
    				number1 += Character.toString(statemen.charAt(ind-i)); 
    				i+=1; 
    			}
    			int indless = ind-i+1; 
    			if (statemen.substring(ind, ind+1).equals("-")) {
    			    ind+=1;
    			}
    			while (ind+m<statemen.length() && (isDigit (Character.toString(statemen.charAt(ind+m)))
    				|| Character.toString(statemen.charAt(ind+m)).equals("."))) {
    				number2 += Character.toString(statemen.charAt(ind+m)); 
    				m+=1; 
    			}
    			int indmore = ind+m; 
    			
    				System.out.println(number1);	
    				System.out.println(number2);	
    				System.out.println(indless);	
    				System.out.println(indmore);
    				System.out.println(statemen.substring(indless, indmore));
    				String mult = statemen.substring(indless, indmore);
    				String multsign = statemen.substring(indless, indmore);
    				String firstsign = "no";
    				if (indless-1>=0) {
    				firstsign =statemen.substring(indless-1, indless);
    				multsign = statemen.substring(indless-1, indmore);
    				}
    				String [] multA=mult.split("[^0-9.]"); 
    				System.out.println(Arrays.toString(multA));
    				String resultA="";
    				try {
    				if (sign == "*") {
    					result = Double.parseDouble(multA[0])*Double.parseDouble(multA[1]);
    					resultA = Double.toString(result);
    				}
    				else if (sign == "/") {
    				    if (Double.parseDouble(multA[1])==0) {
    					return null; 
    				    }
    					result = Double.parseDouble(multA[0])/Double.parseDouble(multA[1]);
    					resultA = Double.toString(result);
    				}
    				else if (sign == "+") {
    				    if (firstsign.equals("-")) {
    					result = Double.parseDouble(multA[1])-Double.parseDouble(multA[0]);
    				    }
    				    else {
    					result = Double.parseDouble(multA[0])+Double.parseDouble(multA[1]); }
    				    if (result>0) {
					resultA = "+"+ Double.toString(result);
					}
    				    else {
    					resultA = Double.toString(result);
    				    }
    				}
    				else if (sign == "-") {
    				    if (firstsign.equals("-")) {
    					result = -Double.parseDouble(multA[0])-Double.parseDouble(multA[1]);
    				    }
    				    else {    				
    					result = Double.parseDouble(multA[0])-Double.parseDouble(multA[1]); }
    				if (result>0) {
					resultA = "+"+ Double.toString(result);
					}
				    else {
					resultA = Double.toString(result);
				    }
    				}
    				}
    				catch (Exception NullPointerException) {
    				    throw null; 
    				}
    				
    				
    				//System.out.printf ("%.3f", result);
    				
    				//DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
    				//String formattedDouble = new DecimalFormat("#0.0000",otherSymbols).format(result);
    				
    				
    				if (sign=="*" || sign=="/" || firstsign=="no") {
    				newst = statemen.replace(mult, resultA); 
    				}
    				else {
    				newst = statemen.replace(multsign, resultA);
    				}
    				/*
    				if (!formattedDouble.substring(0,1).equals("-")) {
    				formattedDouble = firstsign+formattedDouble;
    				}
    				
    				newst = statemen.replace(multsign, formattedDouble); 
    				*/
    				System.out.println(newst);
    				
    	}
    		return newst; 
	}
	
	
    public String evaluate(String statement) {
        // TODO: Implement the logic here
	//String regex = "((-|\\+|/|\\*)?\\d+)+";
	//System.out.println ("-154+5-7*5/654".matches(regex));
	
    	try {
    	if (statement.contains(",")) {
	    return null; 
	}
    	while (statement.indexOf('(')>=0 && statement.indexOf(')', statement.indexOf('('))>0) {
    		statement = brackets (statement);
    		System.out.println(statement);
    		
    	}
    	while (statement.indexOf("/")>0) {
		statement = oneStep (statement, "/");
		System.out.println(statement);
	}
    	while (statement.indexOf("*")>0) {
    		statement = oneStep (statement, "*");
    		System.out.println(statement);
    	}
    	
    	while (statement.indexOf("-")>0 ) {
		statement = oneStep (statement, "-");
		System.out.println(statement);
	}
    	while (statement.indexOf("+")>0) {
    		statement = oneStep (statement, "+");
    		System.out.println(statement);
    	}
    	
    	if (statement.substring(0, 1).equals("+")) {
    	    statement = statement.substring(1);
    	}
    	if (statement.substring(statement.length()-2).equals(".0")){
    	    statement=statement.replace(".0", "");
    	}
    	}
    	catch (Exception NullPointerException) {
    	    return null;
    	}
    	return statement;
}
    	
    	private String brackets (String expression) {
    	    StringBuffer sb= new StringBuffer (expression); 
    	String expressionB = expression.substring(expression.indexOf('(')+1, expression.indexOf(')', expression.indexOf('(')));
    	String expressionBr = "("+expressionB+")"; 
    	    String bracket = evaluate(expressionB); 
    	    if (bracket.substring(0, 1).equals("-")) {
    	int ind = expression.indexOf('(')-1;
		int i=1;
		int m=1;
		while (ind-i>=0 && 
			(isDigit (Character.toString(expression.charAt(ind-i))) 
			|| Character.toString(expression.charAt(ind-i)).equals("."))) {
			i+=1; 
		}
		int indless = ind-i+1; 
		if (indless==0) {
		    expression = "-"+expression; 
		    
		}
		else if (expression.substring(indless-1, indless).equals("-")) {
		    sb.setCharAt(indless-1, '+');
		}
		else {
		    sb.setCharAt(indless-1, '-');
		}
		bracket = bracket.substring(1);
    	    }
    	String newExpr = expression.replace(expressionBr, bracket);
    	
    		return newExpr; 
    	}
    	
    public static void main(String[] args) {
    	Calculator c= new Calculator();
    	
    	//System.out.println(c.evaluate("10/(2-7+3)*4"));
    	System.out.println(c.evaluate(null));
    	//System.out.println(c.evaluate ("22/3*3.0480"));
    	
    }
    

    
}